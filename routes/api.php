<?php

use Illuminate\Support\Facades\Route;

Route::get('/article/{article}','Api\ArticleController@show')->name('api.v1.articles.show');
